import {DomListener} from '@core/DomListener';

export class ExcelComponent extends DomListener {
  constructor($root, options = {}) {
    super($root, options.listeners);
    this.name = options.name || ''
    this.emitter = options.emitter
    this.subscribe = options.subscribe || []
    this.store = options.store
    this.unsubscribe = []
    this.prepare()
  }

  prepare() {

  }

  // Вертає шаблон компонента
  toHTML() {
    return ''
  }

  $emit(event, ...args) {
    this.emitter.emit(event, ...args)
  }

  $on(event, fn) {
    const unSub = this.emitter.subscribe(event, fn)
    this.unsubscribe.push(unSub)
  }

  init() {
    this.initDomListeners()
  }

  isWatching(key) {
    return this.subscribe.includes(key)
  }

  $dispatch(action) {
    this.store.dispatch(action)
  }

  storeChanged() {

  }

  destroy() {
    this.removeDomListeners()
    this.unsubscribe.forEach(unSub => unSub() )
  }
}